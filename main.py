from aiogram import executor, Dispatcher
import loader
import handlers


if __name__ == "__main__":
    executor.start_polling(loader.dp, skip_updates=True)