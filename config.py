from dotenv import load_dotenv
import os

load_dotenv()

TG_TOKEN = str(os.getenv("TG_TOKEN"))