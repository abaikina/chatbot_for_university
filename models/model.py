import random
import logging
from models.text_preprocessing import Text_preprocessing
from sklearn.feature_extraction.text import CountVectorizer
import nltk
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.svm import LinearSVC


class Model:  
    """ Класс для обработки запросов пользователя """ 

    def __init__(self, dataset, stop_words_file_path, model_params):
        self.text_preprocessing = Text_preprocessing(stop_words_file_path)
        self.dataset = dataset
        self.model_params = model_params
        self.__load_data()
        print("Model is ready!")
         

    def __load_data(self):
        """
        Загрузка данных и создание модели.
        """
        logging.info("Loading data...")
        dataset = []
        X = []
        Y = []
        for intent, data in self.dataset["intents"].items():
            for example in data["examples"]:
                X.append(self.text_preprocessing.get_preprocessed_text(example))
                Y.append(intent)
                dataset.append([X, Y])
        
        self.vectorizer = CountVectorizer(analyzer="char_wb", ngram_range=(2,3), max_df=0.8)
        vecX = self.vectorizer.fit_transform(X)

        logging.info("Starting training...")

        # модели ниже дают 0.8863 и 0.8674
        if self.model_params["engine"] == "LogisticRegression":
            try:
                self.model  = LogisticRegression(C=self.model_params["C"], class_weight=self.model_params["class_weight"],  max_iter=self.model_params["max_iter"])
            except:
                self.model  = LogisticRegression(C=1, class_weight="balanced",  max_iter=10000)
        else:
            try:
                self.model  = LinearSVC(C=self.model_params["C"], class_weight=self.model_params["class_weight"],  max_iter=self.model_params["max_iter"])
            except:
                self.model = LinearSVC(C=1, class_weight=None, max_iter=10000)

        # # модель ниже дает 0.8295 и 0.8674
        # self.model  = LogisticRegression(C=1, class_weight="balanced",  max_iter=10000)
        
        # # модель ниже дает 0.8863 и 0.8313
        # self.model = LinearSVC(C=1, class_weight="balanced", max_iter=10000)
        
        X_train, X_test, Y_train, Y_test = train_test_split(vecX, Y, test_size=0.33, stratify=Y, random_state=4)
        self.model.fit(X_train.toarray(), Y_train)

        # Оценка модели на тестовых данных
        accuracy = self.model.score(X_test, Y_test)
        logging.info(f"Done training. Accuracy: {accuracy}")  


    async def get_answer(self, text):
        """
        Возвращает ответ на вопрос.
        """
        new_text = self.text_preprocessing.get_preprocessed_text(text)

        intent = await self.__get_intent_by_ML(new_text)

        if not intent:
            intent = await self.__get_intent_by_Levenshtein(new_text)
        if intent: 
            return random.choice(self.dataset["intents"][intent]["responses"])
            
        return random.choice(self.dataset["failure_phrases"])


    async def __get_intent_by_ML(self, text):
        """
        Поиск интента с помощью модели.
        """
        transformed_text = self.vectorizer.transform([text])
        intent = self.model.predict(transformed_text)[0]
        return intent


    async def __get_intent_by_Levenshtein(self, text):
        """
        Поиск интента с помощью алгоритма Левенштейна.
        """
        for example, intent in self.dataset:
            if await self.__match(text, example):
                return intent
        return ""


    async def __match(self, text, example):
        """
        Проверяет совпадение вопроса и интента в алгоритме Левенштейна.
        """
        distance = nltk.edit_distance(text, example) / len(example)  #  0..1+
        return distance < 0.2

