import string
import pymorphy2
from pyaspeller import YandexSpeller


class Text_preprocessing:
    """ Класс для предобработки текста """

    def __init__(self, stop_words_file_path):  
        self.stop_words_file_path = stop_words_file_path    
        # создание экземпляра класса для морфологического анализа
        self.morph = pymorphy2.MorphAnalyzer()
    

    def get_preprocessed_text(self, text):
        """
        Функция принимает строку и поочередно вызывает функции 
        для ее предобработки; возвращает список слов.
        """
        # corrected_text = self.correct_spelling(text)
        tokenized_text = self.tokenize(text)
        # text_without_stop_words = self.get_text_without_stop_words(tokenized_text)
        lemmatized_text = self.get_lemmatized_text(tokenized_text)

        return lemmatized_text


    def correct_spelling(self, text):
        """
        Функция принимает строку, исправляет грамматические ошибки в строке.
        Возвращает строку.
        """
        speller = YandexSpeller()
        errors = {error['word']: error['s'][0] for error in speller.spell(text)}
        for word, replacement in errors.items():
            text = text.replace(word, replacement)
        return text


    def tokenize(self, text):
        """
        Функция принимает строку, удаляет знаки препинания, уменьшает регистр и разделяет ее на токены.
        Возвращает список слов.
        """
        table = str.maketrans("", "", string.punctuation)
        text = text.lower()
        new_text = text.translate(table)
        list_of_words = new_text.split()

        return list_of_words


    def get_text_without_stop_words(self, list_of_words):
        """
        Функция принимает список слов и удаляет из них стоп-слова (шум).
        Возвращает список слов.
        """
        try:
            # не удалять стоп-слова, если в списке меньше 2-х слов
            if len(list_of_words)<2:
                text_without_stop_words = list_of_words
            else:
                # получение множества стоп-слов из файла stop_words.txt
                stop_words = {word.rstrip('\n') for word in open(self.stop_words_file_path, 'r', encoding="utf-8")}
                text_without_stop_words = [word for word in list_of_words if word not in stop_words]

        except Exception as exception:
            print("\nException: {}".format(type(exception).__name__))
            print("Exception message: {}\n".format(exception))
            text_without_stop_words = list_of_words

        finally:
            return text_without_stop_words


    def get_lemmatized_text(self, list_of_words):
        """
        Функция принимает список слов и приводит их к нормальной форме слова (лемме).
        Возвращает список слов.
        """
        # получение списка слов нормальной формы
        lemmatized_text = " ".join([(self.morph.parse(word)[0]).normal_form for word in list_of_words])

        return lemmatized_text


