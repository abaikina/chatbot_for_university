import os
import logging
from datasets.student_dataset import STUDENT_BOT_CONFIG
from datasets.applicant_dataset import APPLICANT_BOT_CONFIG
from .model import Model


# получение пути до файла со стоп-словами
stop_words_file_path = os.path.join(os.path.dirname(__file__), "stop_words.txt")
# параметры моделей
student_model_params = {"engine":"LinearSVC", "C":1, "class_weight":None, "max_iter":10000}
applicant_model_params = {"engine":"LogisticRegression", "C":1, "class_weight":"balanced", "max_iter":10000}

print("Generating first model...")
logging.info("Generating first model.")
student_model = Model(STUDENT_BOT_CONFIG, stop_words_file_path, student_model_params)

print("Generating second model...")
logging.info("Generating second model.")
applicant_model = Model(APPLICANT_BOT_CONFIG, stop_words_file_path, applicant_model_params)

print("Done!")