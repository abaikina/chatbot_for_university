from aiogram.types import ReplyKeyboardRemove, \
    ReplyKeyboardMarkup, KeyboardButton, \
    InlineKeyboardMarkup, InlineKeyboardButton
from aiogram.utils.callback_data import CallbackData


# клавиатура для возврата
return_kb = ReplyKeyboardMarkup(resize_keyboard=True, row_width = 2)
return_kb.insert("Назад")

# клавиатура для выбора тематики
topic_kb_button_1 = InlineKeyboardButton("Абитуриенты", callback_data="applicant")
topic_kb_button_2 = InlineKeyboardButton("Студенты", callback_data="student")
topic_kb = InlineKeyboardMarkup(row_width = 2).add(topic_kb_button_1).add(topic_kb_button_2)


