import logging
from aiogram import Bot, Dispatcher, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage

from config import TG_TOKEN


logging.basicConfig(
    format=u"%(filename)s [LINE:%(lineno)d] #%(levelname)-8s [%(asctime)s]  %(message)s", 
    level=logging.INFO,
    filename="bot.log",
    filemode = "a",
    )

if not TG_TOKEN:
    logging.error("No token provided.")
    exit()

bot = Bot(TG_TOKEN, parse_mode=types.ParseMode.HTML)
storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)
