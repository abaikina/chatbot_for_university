from aiogram import types
from aiogram.dispatcher import FSMContext
from keyboards import keyboards as kb
from loader import dp


@dp.message_handler(lambda message: message.text.lower() == "назад", state="*")
async def process_step_back(message: types.Message, state: FSMContext):
    """ функция для обработки нажатия кнопки Назад """
    await message.answer("Выберите интересующий Вас пункт:", reply_markup=kb.topic_kb)
    await state.finish()

        
