from aiogram import types
from loader import dp
from models.get_models import student_model
from models.get_models import applicant_model
from keyboards import keyboards as kb
from aiogram.dispatcher import FSMContext
from states.take_state import Topic


@dp.callback_query_handler(text="applicant", state="*")
async def process_applicant_button(call: types.CallbackQuery, state: FSMContext):
    """ отлавливает нажатие кнопки выбора раздела абитуриентов (поступление) """
    await call.answer(cache_time=60)
    await call.message.answer("Я помогу Вам получить необходимую информацию по поступлению на направления нашей кафедры.\n\nПожалуйста, задайте Ваш вопрос.")
    await Topic.applicant.set()


@dp.callback_query_handler(text="student", state="*")
async def process_student_button(call: types.CallbackQuery, state: FSMContext):
    """ отлавливает нажатие кнопки выбора раздела студентов (практики) """
    await call.answer(cache_time=60)
    await call.message.answer("Я помогу Вам получить необходимую информацию по практикам, которые проходят студенты нашей кафедры в процессе обучения.\n\nПожалуйста, задайте Ваш вопрос.")
    await Topic.student.set()


@dp.message_handler(state=Topic.applicant)
async def process_question_from_applicant(message: types.Message, state: FSMContext):
    """ функция для обработки вопроса о поступлении """
    text = await applicant_model.get_answer(message.text)
    await message.answer(text, reply_markup=kb.return_kb)


@dp.message_handler(state=Topic.student)
async def process_question_from_student(message: types.Message, state: FSMContext):
    """ функция для обработки вопроса о практиках """
    text = await student_model.get_answer(message.text)
    await message.answer(text, reply_markup=kb.return_kb)


@dp.message_handler(state='*')
async def process_message(message: types.Message, state: FSMContext):
    """ функция для обработки другого текста """
    await message.reply("Неизвестная команда.", reply_markup=kb.return_kb)
    await message.answer("Выберите интересующий Вас пункт:", reply_markup=kb.topic_kb)
    await state.finish()