from aiogram import types
from loader import dp
from keyboards import keyboards as kb
from aiogram.dispatcher import FSMContext


@dp.message_handler(commands=["start"], state="*")
async def process_command_start(message: types.Message, state: FSMContext):
    """ функция для обработки команды /start """
    await message.answer("Вас приветствует чатбот кафедры <b>«Информатика и системы управления»</b> НГТУ им. Р.Е. Алексеева.\n\nКафедра осуществляет подготовку бакалавров и магистров в области построения интеллектуальных систем обработки информации и управления, использования современных информационных технологий, в среде <i>MatLab</i>, работы с библиотеками компьютерного зрения <i>OpenCV</i> и <i>OpenVINO</i>, разработки баз данных, цифровой обработки сигналов, программирования микропроцессорных устройств контроля и управления, обеспечения информационной безопасности.\n\nНаши <i>студенты</i> активно занимаются научноисследовательской работой, выступают с докладами на конференциях, успешно участвуют в конкурсах и хакатонах, - за свои достижения получают награды и именные стипендии.", reply_markup=kb.return_kb)
    await message.answer("Выберите интересующий Вас пункт:", reply_markup=kb.topic_kb)
    await state.finish()


@dp.message_handler(commands=["help"], state="*")
async def process_command_help(message: types.Message, state: FSMContext):
    """ функция для обработки команды /help """
    await message.answer("<b>Доступные команды</b>:\n\n/start - Запустить/Перезапустить бота\n/help - Вывести справку", reply_markup=kb.return_kb)
    await state.finish()
